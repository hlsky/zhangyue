package com.ruoyi.project.system.city.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.city.domain.ZjCity;
import com.ruoyi.project.system.city.service.IZjCityService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 城市Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/city")
public class ZjCityController extends BaseController
{
    private String prefix = "system/city";

    @Autowired
    private IZjCityService zjCityService;

    @RequiresPermissions("system:city:view")
    @GetMapping()
    public String city()
    {
        return prefix + "/city";
    }

    /**
     * 查询城市列表
     */
    @RequiresPermissions("system:city:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjCity zjCity)
    {
        startPage();
        List<ZjCity> list = zjCityService.selectZjCityList(zjCity);
        return getDataTable(list);
    }

    /**
     * 导出城市列表
     */
    @RequiresPermissions("system:city:export")
    @Log(title = "城市", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjCity zjCity)
    {
        List<ZjCity> list = zjCityService.selectZjCityList(zjCity);
        ExcelUtil<ZjCity> util = new ExcelUtil<ZjCity>(ZjCity.class);
        return util.exportExcel(list, "city");
    }

    /**
     * 新增城市
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存城市
     */
    @RequiresPermissions("system:city:add")
    @Log(title = "城市", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjCity zjCity)
    {
        return toAjax(zjCityService.insertZjCity(zjCity));
    }

    /**
     * 修改城市
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjCity zjCity = zjCityService.selectZjCityById(id);
        mmap.put("zjCity", zjCity);
        return prefix + "/edit";
    }

    /**
     * 修改保存城市
     */
    @RequiresPermissions("system:city:edit")
    @Log(title = "城市", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjCity zjCity)
    {
        return toAjax(zjCityService.updateZjCity(zjCity));
    }

    /**
     * 删除城市
     */
    @RequiresPermissions("system:city:remove")
    @Log(title = "城市", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjCityService.deleteZjCityByIds(ids));
    }
}
