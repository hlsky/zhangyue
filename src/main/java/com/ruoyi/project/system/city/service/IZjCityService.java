package com.ruoyi.project.system.city.service;

import java.util.List;
import com.ruoyi.project.system.city.domain.ZjCity;

/**
 * 城市Service接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface IZjCityService 
{
    /**
     * 查询城市
     * 
     * @param id 城市ID
     * @return 城市
     */
    public ZjCity selectZjCityById(Long id);

    /**
     * 查询城市列表
     * 
     * @param zjCity 城市
     * @return 城市集合
     */
    public List<ZjCity> selectZjCityList(ZjCity zjCity);

    /**
     * 新增城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    public int insertZjCity(ZjCity zjCity);

    /**
     * 修改城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    public int updateZjCity(ZjCity zjCity);

    /**
     * 批量删除城市
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjCityByIds(String ids);

    /**
     * 删除城市信息
     * 
     * @param id 城市ID
     * @return 结果
     */
    public int deleteZjCityById(Long id);
}
