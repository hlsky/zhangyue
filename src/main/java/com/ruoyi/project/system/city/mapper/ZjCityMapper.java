package com.ruoyi.project.system.city.mapper;

import java.util.List;
import com.ruoyi.project.system.city.domain.ZjCity;

/**
 * 城市Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface ZjCityMapper 
{
    /**
     * 查询城市
     * 
     * @param id 城市ID
     * @return 城市
     */
    public ZjCity selectZjCityById(Long id);

    /**
     * 查询城市列表
     * 
     * @param zjCity 城市
     * @return 城市集合
     */
    public List<ZjCity> selectZjCityList(ZjCity zjCity);

    /**
     * 新增城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    public int insertZjCity(ZjCity zjCity);

    /**
     * 修改城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    public int updateZjCity(ZjCity zjCity);

    /**
     * 删除城市
     * 
     * @param id 城市ID
     * @return 结果
     */
    public int deleteZjCityById(Long id);

    /**
     * 批量删除城市
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjCityByIds(String[] ids);
}
