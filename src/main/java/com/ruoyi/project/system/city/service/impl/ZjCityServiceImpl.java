package com.ruoyi.project.system.city.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.city.mapper.ZjCityMapper;
import com.ruoyi.project.system.city.domain.ZjCity;
import com.ruoyi.project.system.city.service.IZjCityService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 城市Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjCityServiceImpl implements IZjCityService 
{
    @Autowired
    private ZjCityMapper zjCityMapper;

    /**
     * 查询城市
     * 
     * @param id 城市ID
     * @return 城市
     */
    @Override
    public ZjCity selectZjCityById(Long id)
    {
        return zjCityMapper.selectZjCityById(id);
    }

    /**
     * 查询城市列表
     * 
     * @param zjCity 城市
     * @return 城市
     */
    @Override
    public List<ZjCity> selectZjCityList(ZjCity zjCity)
    {
        return zjCityMapper.selectZjCityList(zjCity);
    }

    /**
     * 新增城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    @Override
    public int insertZjCity(ZjCity zjCity)
    {
        return zjCityMapper.insertZjCity(zjCity);
    }

    /**
     * 修改城市
     * 
     * @param zjCity 城市
     * @return 结果
     */
    @Override
    public int updateZjCity(ZjCity zjCity)
    {
        return zjCityMapper.updateZjCity(zjCity);
    }

    /**
     * 删除城市对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjCityByIds(String ids)
    {
        return zjCityMapper.deleteZjCityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除城市信息
     * 
     * @param id 城市ID
     * @return 结果
     */
    @Override
    public int deleteZjCityById(Long id)
    {
        return zjCityMapper.deleteZjCityById(id);
    }
}
