package com.ruoyi.project.system.province.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.province.domain.ZjProvince;
import com.ruoyi.project.system.province.service.IZjProvinceService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 省份Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/province")
public class ZjProvinceController extends BaseController
{
    private String prefix = "system/province";

    @Autowired
    private IZjProvinceService zjProvinceService;

    @RequiresPermissions("system:province:view")
    @GetMapping()
    public String province()
    {
        return prefix + "/province";
    }

    /**
     * 查询省份列表
     */
    @RequiresPermissions("system:province:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjProvince zjProvince)
    {
        startPage();
        List<ZjProvince> list = zjProvinceService.selectZjProvinceList(zjProvince);
        return getDataTable(list);
    }

    /**
     * 导出省份列表
     */
    @RequiresPermissions("system:province:export")
    @Log(title = "省份", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjProvince zjProvince)
    {
        List<ZjProvince> list = zjProvinceService.selectZjProvinceList(zjProvince);
        ExcelUtil<ZjProvince> util = new ExcelUtil<ZjProvince>(ZjProvince.class);
        return util.exportExcel(list, "province");
    }

    /**
     * 新增省份
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存省份
     */
    @RequiresPermissions("system:province:add")
    @Log(title = "省份", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjProvince zjProvince)
    {
        return toAjax(zjProvinceService.insertZjProvince(zjProvince));
    }

    /**
     * 修改省份
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjProvince zjProvince = zjProvinceService.selectZjProvinceById(id);
        mmap.put("zjProvince", zjProvince);
        return prefix + "/edit";
    }

    /**
     * 修改保存省份
     */
    @RequiresPermissions("system:province:edit")
    @Log(title = "省份", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjProvince zjProvince)
    {
        return toAjax(zjProvinceService.updateZjProvince(zjProvince));
    }

    /**
     * 删除省份
     */
    @RequiresPermissions("system:province:remove")
    @Log(title = "省份", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjProvinceService.deleteZjProvinceByIds(ids));
    }
}
