package com.ruoyi.project.system.province.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.province.mapper.ZjProvinceMapper;
import com.ruoyi.project.system.province.domain.ZjProvince;
import com.ruoyi.project.system.province.service.IZjProvinceService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 省份Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjProvinceServiceImpl implements IZjProvinceService 
{
    @Autowired
    private ZjProvinceMapper zjProvinceMapper;

    /**
     * 查询省份
     * 
     * @param id 省份ID
     * @return 省份
     */
    @Override
    public ZjProvince selectZjProvinceById(Long id)
    {
        return zjProvinceMapper.selectZjProvinceById(id);
    }

    /**
     * 查询省份列表
     * 
     * @param zjProvince 省份
     * @return 省份
     */
    @Override
    public List<ZjProvince> selectZjProvinceList(ZjProvince zjProvince)
    {
        return zjProvinceMapper.selectZjProvinceList(zjProvince);
    }

    /**
     * 新增省份
     * 
     * @param zjProvince 省份
     * @return 结果
     */
    @Override
    public int insertZjProvince(ZjProvince zjProvince)
    {
        return zjProvinceMapper.insertZjProvince(zjProvince);
    }

    /**
     * 修改省份
     * 
     * @param zjProvince 省份
     * @return 结果
     */
    @Override
    public int updateZjProvince(ZjProvince zjProvince)
    {
        return zjProvinceMapper.updateZjProvince(zjProvince);
    }

    /**
     * 删除省份对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjProvinceByIds(String ids)
    {
        return zjProvinceMapper.deleteZjProvinceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除省份信息
     * 
     * @param id 省份ID
     * @return 结果
     */
    @Override
    public int deleteZjProvinceById(Long id)
    {
        return zjProvinceMapper.deleteZjProvinceById(id);
    }
}
