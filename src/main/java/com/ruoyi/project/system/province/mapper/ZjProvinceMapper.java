package com.ruoyi.project.system.province.mapper;

import java.util.List;
import com.ruoyi.project.system.province.domain.ZjProvince;

/**
 * 省份Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface ZjProvinceMapper 
{
    /**
     * 查询省份
     * 
     * @param id 省份ID
     * @return 省份
     */
    public ZjProvince selectZjProvinceById(Long id);

    /**
     * 查询省份列表
     * 
     * @param zjProvince 省份
     * @return 省份集合
     */
    public List<ZjProvince> selectZjProvinceList(ZjProvince zjProvince);

    /**
     * 新增省份
     * 
     * @param zjProvince 省份
     * @return 结果
     */
    public int insertZjProvince(ZjProvince zjProvince);

    /**
     * 修改省份
     * 
     * @param zjProvince 省份
     * @return 结果
     */
    public int updateZjProvince(ZjProvince zjProvince);

    /**
     * 删除省份
     * 
     * @param id 省份ID
     * @return 结果
     */
    public int deleteZjProvinceById(Long id);

    /**
     * 批量删除省份
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjProvinceByIds(String[] ids);
}
