package com.ruoyi.project.system.card.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.card.domain.ZjBaseCard;
import com.ruoyi.project.system.card.service.IZjBaseCardService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/card")
public class ZjBaseCardController extends BaseController
{
    private String prefix = "system/card";

    @Autowired
    private IZjBaseCardService zjBaseCardService;

    @RequiresPermissions("system:card:view")
    @GetMapping()
    public String card()
    {
        return prefix + "/card";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:card:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjBaseCard zjBaseCard)
    {
        startPage();
        List<ZjBaseCard> list = zjBaseCardService.selectZjBaseCardList(zjBaseCard);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:card:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjBaseCard zjBaseCard)
    {
        List<ZjBaseCard> list = zjBaseCardService.selectZjBaseCardList(zjBaseCard);
        ExcelUtil<ZjBaseCard> util = new ExcelUtil<ZjBaseCard>(ZjBaseCard.class);
        return util.exportExcel(list, "card");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:card:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjBaseCard zjBaseCard)
    {
        return toAjax(zjBaseCardService.insertZjBaseCard(zjBaseCard));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjBaseCard zjBaseCard = zjBaseCardService.selectZjBaseCardById(id);
        mmap.put("zjBaseCard", zjBaseCard);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:card:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjBaseCard zjBaseCard)
    {
        return toAjax(zjBaseCardService.updateZjBaseCard(zjBaseCard));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:card:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjBaseCardService.deleteZjBaseCardByIds(ids));
    }
}
