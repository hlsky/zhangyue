package com.ruoyi.project.system.card.service;

import java.util.List;
import com.ruoyi.project.system.card.domain.ZjBaseCard;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface IZjBaseCardService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public ZjBaseCard selectZjBaseCardById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZjBaseCard> selectZjBaseCardList(ZjBaseCard zjBaseCard);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 结果
     */
    public int insertZjBaseCard(ZjBaseCard zjBaseCard);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 结果
     */
    public int updateZjBaseCard(ZjBaseCard zjBaseCard);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjBaseCardByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteZjBaseCardById(Long id);
}
