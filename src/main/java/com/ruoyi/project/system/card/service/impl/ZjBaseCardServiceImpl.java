package com.ruoyi.project.system.card.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.card.mapper.ZjBaseCardMapper;
import com.ruoyi.project.system.card.domain.ZjBaseCard;
import com.ruoyi.project.system.card.service.IZjBaseCardService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjBaseCardServiceImpl implements IZjBaseCardService 
{
    @Autowired
    private ZjBaseCardMapper zjBaseCardMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public ZjBaseCard selectZjBaseCardById(Long id)
    {
        return zjBaseCardMapper.selectZjBaseCardById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZjBaseCard> selectZjBaseCardList(ZjBaseCard zjBaseCard)
    {
        return zjBaseCardMapper.selectZjBaseCardList(zjBaseCard);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZjBaseCard(ZjBaseCard zjBaseCard)
    {
        return zjBaseCardMapper.insertZjBaseCard(zjBaseCard);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zjBaseCard 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZjBaseCard(ZjBaseCard zjBaseCard)
    {
        return zjBaseCardMapper.updateZjBaseCard(zjBaseCard);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseCardByIds(String ids)
    {
        return zjBaseCardMapper.deleteZjBaseCardByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseCardById(Long id)
    {
        return zjBaseCardMapper.deleteZjBaseCardById(id);
    }
}
