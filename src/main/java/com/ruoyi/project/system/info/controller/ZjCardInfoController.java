package com.ruoyi.project.system.info.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.info.domain.ZjCardInfo;
import com.ruoyi.project.system.info.service.IZjCardInfoService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 专业信息Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/cardinfo")
public class ZjCardInfoController extends BaseController
{
    private String prefix = "system/info";

    @Autowired
    private IZjCardInfoService zjCardInfoService;

    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }

    /**
     * 查询专业信息列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjCardInfo zjCardInfo)
    {
        startPage();
        List<ZjCardInfo> list = zjCardInfoService.selectZjCardInfoList(zjCardInfo);
        return getDataTable(list);
    }

    /**
     * 导出专业信息列表
     */
    @RequiresPermissions("system:info:export")
    @Log(title = "专业信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjCardInfo zjCardInfo)
    {
        List<ZjCardInfo> list = zjCardInfoService.selectZjCardInfoList(zjCardInfo);
        ExcelUtil<ZjCardInfo> util = new ExcelUtil<ZjCardInfo>(ZjCardInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 新增专业信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存专业信息
     */
    @RequiresPermissions("system:info:add")
    @Log(title = "专业信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjCardInfo zjCardInfo)
    {
        return toAjax(zjCardInfoService.insertZjCardInfo(zjCardInfo));
    }

    /**
     * 修改专业信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjCardInfo zjCardInfo = zjCardInfoService.selectZjCardInfoById(id);
        mmap.put("zjCardInfo", zjCardInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存专业信息
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "专业信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjCardInfo zjCardInfo)
    {
        return toAjax(zjCardInfoService.updateZjCardInfo(zjCardInfo));
    }

    /**
     * 删除专业信息
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "专业信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjCardInfoService.deleteZjCardInfoByIds(ids));
    }
}
