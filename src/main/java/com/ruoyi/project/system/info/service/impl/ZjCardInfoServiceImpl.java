package com.ruoyi.project.system.info.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.info.mapper.ZjCardInfoMapper;
import com.ruoyi.project.system.info.domain.ZjCardInfo;
import com.ruoyi.project.system.info.service.IZjCardInfoService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 专业信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjCardInfoServiceImpl implements IZjCardInfoService 
{
    @Autowired
    private ZjCardInfoMapper zjCardInfoMapper;

    /**
     * 查询专业信息
     * 
     * @param id 专业信息ID
     * @return 专业信息
     */
    @Override
    public ZjCardInfo selectZjCardInfoById(Long id)
    {
        return zjCardInfoMapper.selectZjCardInfoById(id);
    }

    /**
     * 查询专业信息列表
     * 
     * @param zjCardInfo 专业信息
     * @return 专业信息
     */
    @Override
    public List<ZjCardInfo> selectZjCardInfoList(ZjCardInfo zjCardInfo)
    {
        return zjCardInfoMapper.selectZjCardInfoList(zjCardInfo);
    }

    /**
     * 新增专业信息
     * 
     * @param zjCardInfo 专业信息
     * @return 结果
     */
    @Override
    public int insertZjCardInfo(ZjCardInfo zjCardInfo)
    {
        return zjCardInfoMapper.insertZjCardInfo(zjCardInfo);
    }

    /**
     * 修改专业信息
     * 
     * @param zjCardInfo 专业信息
     * @return 结果
     */
    @Override
    public int updateZjCardInfo(ZjCardInfo zjCardInfo)
    {
        return zjCardInfoMapper.updateZjCardInfo(zjCardInfo);
    }

    /**
     * 删除专业信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjCardInfoByIds(String ids)
    {
        return zjCardInfoMapper.deleteZjCardInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除专业信息信息
     * 
     * @param id 专业信息ID
     * @return 结果
     */
    @Override
    public int deleteZjCardInfoById(Long id)
    {
        return zjCardInfoMapper.deleteZjCardInfoById(id);
    }
}
