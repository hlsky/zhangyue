package com.ruoyi.project.system.info.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.info.mapper.ZjBaseInfoMapper;
import com.ruoyi.project.system.info.domain.ZjBaseInfo;
import com.ruoyi.project.system.info.service.IZjBaseInfoService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 专家库Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjBaseInfoServiceImpl implements IZjBaseInfoService 
{
    @Autowired
    private ZjBaseInfoMapper zjBaseInfoMapper;

    /**
     * 查询专家库
     * 
     * @param id 专家库ID
     * @return 专家库
     */
    @Override
    public ZjBaseInfo selectZjBaseInfoById(Long id)
    {
        return zjBaseInfoMapper.selectZjBaseInfoById(id);
    }

    /**
     * 查询专家库列表
     * 
     * @param zjBaseInfo 专家库
     * @return 专家库
     */
    @Override
    public List<ZjBaseInfo> selectZjBaseInfoList(ZjBaseInfo zjBaseInfo)
    {
        return zjBaseInfoMapper.selectZjBaseInfoList(zjBaseInfo);
    }

    /**
     * 新增专家库
     * 
     * @param zjBaseInfo 专家库
     * @return 结果
     */
    @Override
    public int insertZjBaseInfo(ZjBaseInfo zjBaseInfo)
    {
        return zjBaseInfoMapper.insertZjBaseInfo(zjBaseInfo);
    }

    /**
     * 修改专家库
     * 
     * @param zjBaseInfo 专家库
     * @return 结果
     */
    @Override
    public int updateZjBaseInfo(ZjBaseInfo zjBaseInfo)
    {
        return zjBaseInfoMapper.updateZjBaseInfo(zjBaseInfo);
    }

    /**
     * 删除专家库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseInfoByIds(String ids)
    {
        return zjBaseInfoMapper.deleteZjBaseInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除专家库信息
     * 
     * @param id 专家库ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseInfoById(Long id)
    {
        return zjBaseInfoMapper.deleteZjBaseInfoById(id);
    }
}
