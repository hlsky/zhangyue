package com.ruoyi.project.system.info.mapper;

import java.util.List;
import com.ruoyi.project.system.info.domain.ZjBaseInfo;

/**
 * 专家库Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface ZjBaseInfoMapper 
{
    /**
     * 查询专家库
     * 
     * @param id 专家库ID
     * @return 专家库
     */
    public ZjBaseInfo selectZjBaseInfoById(Long id);

    /**
     * 查询专家库列表
     * 
     * @param zjBaseInfo 专家库
     * @return 专家库集合
     */
    public List<ZjBaseInfo> selectZjBaseInfoList(ZjBaseInfo zjBaseInfo);

    /**
     * 新增专家库
     * 
     * @param zjBaseInfo 专家库
     * @return 结果
     */
    public int insertZjBaseInfo(ZjBaseInfo zjBaseInfo);

    /**
     * 修改专家库
     * 
     * @param zjBaseInfo 专家库
     * @return 结果
     */
    public int updateZjBaseInfo(ZjBaseInfo zjBaseInfo);

    /**
     * 删除专家库
     * 
     * @param id 专家库ID
     * @return 结果
     */
    public int deleteZjBaseInfoById(Long id);

    /**
     * 批量删除专家库
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjBaseInfoByIds(String[] ids);
}
