package com.ruoyi.project.system.info.mapper;

import java.util.List;
import com.ruoyi.project.system.info.domain.ZjCardInfo;

/**
 * 专业信息Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface ZjCardInfoMapper 
{
    /**
     * 查询专业信息
     * 
     * @param id 专业信息ID
     * @return 专业信息
     */
    public ZjCardInfo selectZjCardInfoById(Long id);

    /**
     * 查询专业信息列表
     * 
     * @param zjCardInfo 专业信息
     * @return 专业信息集合
     */
    public List<ZjCardInfo> selectZjCardInfoList(ZjCardInfo zjCardInfo);

    /**
     * 新增专业信息
     * 
     * @param zjCardInfo 专业信息
     * @return 结果
     */
    public int insertZjCardInfo(ZjCardInfo zjCardInfo);

    /**
     * 修改专业信息
     * 
     * @param zjCardInfo 专业信息
     * @return 结果
     */
    public int updateZjCardInfo(ZjCardInfo zjCardInfo);

    /**
     * 删除专业信息
     * 
     * @param id 专业信息ID
     * @return 结果
     */
    public int deleteZjCardInfoById(Long id);

    /**
     * 批量删除专业信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjCardInfoByIds(String[] ids);
}
