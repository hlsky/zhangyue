package com.ruoyi.project.system.info.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.info.domain.ZjBaseInfo;
import com.ruoyi.project.system.info.service.IZjBaseInfoService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 专家库Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/baseinfo")
public class ZjBaseInfoController extends BaseController
{
    private String prefix = "system/info";

    @Autowired
    private IZjBaseInfoService zjBaseInfoService;

    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }

    /**
     * 查询专家库列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjBaseInfo zjBaseInfo)
    {
        startPage();
        List<ZjBaseInfo> list = zjBaseInfoService.selectZjBaseInfoList(zjBaseInfo);
        return getDataTable(list);
    }

    /**
     * 导出专家库列表
     */
    @RequiresPermissions("system:info:export")
    @Log(title = "专家库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjBaseInfo zjBaseInfo)
    {
        List<ZjBaseInfo> list = zjBaseInfoService.selectZjBaseInfoList(zjBaseInfo);
        ExcelUtil<ZjBaseInfo> util = new ExcelUtil<ZjBaseInfo>(ZjBaseInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 新增专家库
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存专家库
     */
    @RequiresPermissions("system:info:add")
    @Log(title = "专家库", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjBaseInfo zjBaseInfo)
    {
        return toAjax(zjBaseInfoService.insertZjBaseInfo(zjBaseInfo));
    }

    /**
     * 修改专家库
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjBaseInfo zjBaseInfo = zjBaseInfoService.selectZjBaseInfoById(id);
        mmap.put("zjBaseInfo", zjBaseInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存专家库
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "专家库", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjBaseInfo zjBaseInfo)
    {
        return toAjax(zjBaseInfoService.updateZjBaseInfo(zjBaseInfo));
    }

    /**
     * 删除专家库
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "专家库", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjBaseInfoService.deleteZjBaseInfoByIds(ids));
    }
}
