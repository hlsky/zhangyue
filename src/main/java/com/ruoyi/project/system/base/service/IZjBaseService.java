package com.ruoyi.project.system.base.service;

import java.util.List;
import com.ruoyi.project.system.base.domain.ZjBase;

/**
 * 专家库Service接口
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public interface IZjBaseService 
{
    /**
     * 查询专家库
     * 
     * @param id 专家库ID
     * @return 专家库
     */
    public ZjBase selectZjBaseById(Long id);

    /**
     * 查询专家库列表
     * 
     * @param zjBase 专家库
     * @return 专家库集合
     */
    public List<ZjBase> selectZjBaseList(ZjBase zjBase);

    /**
     * 新增专家库
     * 
     * @param zjBase 专家库
     * @return 结果
     */
    public int insertZjBase(ZjBase zjBase);

    /**
     * 修改专家库
     * 
     * @param zjBase 专家库
     * @return 结果
     */
    public int updateZjBase(ZjBase zjBase);

    /**
     * 批量删除专家库
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteZjBaseByIds(String ids);

    /**
     * 删除专家库信息
     * 
     * @param id 专家库ID
     * @return 结果
     */
    public int deleteZjBaseById(Long id);
}
