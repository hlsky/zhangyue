package com.ruoyi.project.system.base.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.base.domain.ZjBase;
import com.ruoyi.project.system.base.service.IZjBaseService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 专家库Controller
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Controller
@RequestMapping("/system/base")
public class ZjBaseController extends BaseController
{
    private String prefix = "system/base";

    @Autowired
    private IZjBaseService zjBaseService;

    @RequiresPermissions("system:base:view")
    @GetMapping()
    public String base()
    {
        return prefix + "/base";
    }

    /**
     * 查询专家库列表
     */
    @RequiresPermissions("system:base:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZjBase zjBase)
    {
        startPage();
        List<ZjBase> list = zjBaseService.selectZjBaseList(zjBase);
        return getDataTable(list);
    }

    /**
     * 导出专家库列表
     */
    @RequiresPermissions("system:base:export")
    @Log(title = "专家库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZjBase zjBase)
    {
        List<ZjBase> list = zjBaseService.selectZjBaseList(zjBase);
        ExcelUtil<ZjBase> util = new ExcelUtil<ZjBase>(ZjBase.class);
        return util.exportExcel(list, "base");
    }

    /**
     * 新增专家库
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存专家库
     */
    @RequiresPermissions("system:base:add")
    @Log(title = "专家库", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZjBase zjBase)
    {
        return toAjax(zjBaseService.insertZjBase(zjBase));
    }

    /**
     * 修改专家库
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZjBase zjBase = zjBaseService.selectZjBaseById(id);
        mmap.put("zjBase", zjBase);
        return prefix + "/edit";
    }

    /**
     * 修改保存专家库
     */
    @RequiresPermissions("system:base:edit")
    @Log(title = "专家库", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZjBase zjBase)
    {
        return toAjax(zjBaseService.updateZjBase(zjBase));
    }

    /**
     * 删除专家库
     */
    @RequiresPermissions("system:base:remove")
    @Log(title = "专家库", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zjBaseService.deleteZjBaseByIds(ids));
    }
}
