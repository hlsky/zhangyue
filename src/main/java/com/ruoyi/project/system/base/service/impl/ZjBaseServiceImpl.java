package com.ruoyi.project.system.base.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.base.mapper.ZjBaseMapper;
import com.ruoyi.project.system.base.domain.ZjBase;
import com.ruoyi.project.system.base.service.IZjBaseService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 专家库Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
@Service
public class ZjBaseServiceImpl implements IZjBaseService 
{
    @Autowired
    private ZjBaseMapper zjBaseMapper;

    /**
     * 查询专家库
     * 
     * @param id 专家库ID
     * @return 专家库
     */
    @Override
    public ZjBase selectZjBaseById(Long id)
    {
        return zjBaseMapper.selectZjBaseById(id);
    }

    /**
     * 查询专家库列表
     * 
     * @param zjBase 专家库
     * @return 专家库
     */
    @Override
    public List<ZjBase> selectZjBaseList(ZjBase zjBase)
    {
        return zjBaseMapper.selectZjBaseList(zjBase);
    }

    /**
     * 新增专家库
     * 
     * @param zjBase 专家库
     * @return 结果
     */
    @Override
    public int insertZjBase(ZjBase zjBase)
    {
        return zjBaseMapper.insertZjBase(zjBase);
    }

    /**
     * 修改专家库
     * 
     * @param zjBase 专家库
     * @return 结果
     */
    @Override
    public int updateZjBase(ZjBase zjBase)
    {
        return zjBaseMapper.updateZjBase(zjBase);
    }

    /**
     * 删除专家库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseByIds(String ids)
    {
        return zjBaseMapper.deleteZjBaseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除专家库信息
     * 
     * @param id 专家库ID
     * @return 结果
     */
    @Override
    public int deleteZjBaseById(Long id)
    {
        return zjBaseMapper.deleteZjBaseById(id);
    }
}
