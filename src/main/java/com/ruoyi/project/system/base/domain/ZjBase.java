package com.ruoyi.project.system.base.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 专家库对象 zj_base
 * 
 * @author ruoyi
 * @date 2020-09-22
 */
public class ZjBase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 省份id */
    @Excel(name = "省份id")
    private Long provinceId;

    /** 省份名称 */
    @Excel(name = "省份名称")
    private String provinceName;

    /** 城市id */
    @Excel(name = "城市id")
    private Long cityId;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String cityName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 生日 */
    @Excel(name = "生日")
    private String birthday;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String cardType;

    /** 证件号 */
    @Excel(name = "证件号")
    private String card;

    /** 职称 */
    @Excel(name = "职称")
    private String cardLevel;

    /** 宸ヤ綔鍗曚綅 */
    @Excel(name = "宸ヤ綔鍗曚綅")
    private String unioner;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** 申报专业 */
    @Excel(name = "申报专业")
    private String cardInfo;

    /** 证书编号 */
    @Excel(name = "证书编号")
    private String cardNo;

    /** 发证时间 */
    @Excel(name = "发证时间")
    private String cardTime;

    /** email */
    @Excel(name = "email")
    private String email;

    /** 是否在库 */
    @Excel(name = "是否在库")
    private String orgFlag;

    /** 代理机构名称 */
    @Excel(name = "代理机构名称")
    private String orgName;

    /** 鍙傚姞璇勬爣宸ヤ綔鎯呭喌 */
    @Excel(name = "鍙傚姞璇勬爣宸ヤ綔鎯呭喌")
    private Long lineCount;

    /** 评价情况 */
    @Excel(name = "评价情况")
    private String score;

    /** 是否参加再教育 */
    @Excel(name = "是否参加再教育")
    private String eduCount;

    /** 删除标记 */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProvinceId(Long provinceId)
    {
        this.provinceId = provinceId;
    }

    public Long getProvinceId()
    {
        return provinceId;
    }
    public void setProvinceName(String provinceName)
    {
        this.provinceName = provinceName;
    }

    public String getProvinceName()
    {
        return provinceName;
    }
    public void setCityId(Long cityId)
    {
        this.cityId = cityId;
    }

    public Long getCityId()
    {
        return cityId;
    }
    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getCityName()
    {
        return cityName;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getSex()
    {
        return sex;
    }
    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public String getBirthday()
    {
        return birthday;
    }
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public String getCardType()
    {
        return cardType;
    }
    public void setCard(String card)
    {
        this.card = card;
    }

    public String getCard()
    {
        return card;
    }
    public void setCardLevel(String cardLevel)
    {
        this.cardLevel = cardLevel;
    }

    public String getCardLevel()
    {
        return cardLevel;
    }
    public void setUnioner(String unioner)
    {
        this.unioner = unioner;
    }

    public String getUnioner()
    {
        return unioner;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setCardInfo(String cardInfo)
    {
        this.cardInfo = cardInfo;
    }

    public String getCardInfo()
    {
        return cardInfo;
    }
    public void setCardNo(String cardNo)
    {
        this.cardNo = cardNo;
    }

    public String getCardNo()
    {
        return cardNo;
    }
    public void setCardTime(String cardTime)
    {
        this.cardTime = cardTime;
    }

    public String getCardTime()
    {
        return cardTime;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }
    public void setOrgFlag(String orgFlag)
    {
        this.orgFlag = orgFlag;
    }

    public String getOrgFlag()
    {
        return orgFlag;
    }
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    public String getOrgName()
    {
        return orgName;
    }
    public void setLineCount(Long lineCount)
    {
        this.lineCount = lineCount;
    }

    public Long getLineCount()
    {
        return lineCount;
    }
    public void setScore(String score)
    {
        this.score = score;
    }

    public String getScore()
    {
        return score;
    }
    public void setEduCount(String eduCount)
    {
        this.eduCount = eduCount;
    }

    public String getEduCount()
    {
        return eduCount;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("provinceId", getProvinceId())
            .append("provinceName", getProvinceName())
            .append("cityId", getCityId())
            .append("cityName", getCityName())
            .append("name", getName())
            .append("sex", getSex())
            .append("birthday", getBirthday())
            .append("cardType", getCardType())
            .append("card", getCard())
            .append("cardLevel", getCardLevel())
            .append("unioner", getUnioner())
            .append("phone", getPhone())
            .append("cardInfo", getCardInfo())
            .append("cardNo", getCardNo())
            .append("cardTime", getCardTime())
            .append("email", getEmail())
            .append("orgFlag", getOrgFlag())
            .append("orgName", getOrgName())
            .append("lineCount", getLineCount())
            .append("score", getScore())
            .append("eduCount", getEduCount())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
